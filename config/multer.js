// Multer stuff
const constants = require('./constants');

const multer = require('multer');
const path = require('path');
const fse = require('fs-extra')
// global.UPLOAD_MAX_FILE_SIZE = 1024 * 1024;

let storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        const data = req.body;
        const folder = data.folder;
        const edit = !!data.id;

        // Reflect.ownKeys(req.body).forEach(key => {
        //     data[key] = JSON.parse(req.body[key]);
        // });



        console.log(data, file)

        let dir = constants.USERS_UPLOAD_FOLDER;

        console.log('multer!!!!')



        console.log('dir!!!!!')
        console.log(dir)



        cb(null, dir)
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname) // already have got Date implemented in the name
    }
});


let upload = multer({
    storage: storage,
    // limits: {fileSize: UPLOAD_MAX_FILE_SIZE},
    fileFilter: function (req, file, cb) {
        let filetypes = /jpeg|jpg/;
        let mimetype = filetypes.test(file.mimetype);
        let extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        console.log('uploading!!!!')
        if (!mimetype && !extname) {
            req.fileTypeError = {message: "The file has an invalid type"};
            return cb(null, false, req.fileTypeError)
        }
        cb(null, true);
    }
});
// global.uploadProfileImg = upload.single('profile_img_file');
// global.uploadTourImg = upload.single('upload_image');
uploadImages = upload.array('upload_images');
module.exports = {uploadImages};
