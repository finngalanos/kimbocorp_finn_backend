const path = require('path');

const PUBLIC_FOLDER = path.join(__dirname, '../public/');
const UPLOADS_FOLDER = path.join(__dirname, '../public/uploads');
const USERS_UPLOAD_FOLDER = path.join(UPLOADS_FOLDER, 'users/');

// console.log(process.env)



module.exports = {PUBLIC_FOLDER, UPLOADS_FOLDER, USERS_UPLOAD_FOLDER};
