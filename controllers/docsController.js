const sequelize = require('sequelize');
const db = require('../models');
const Users = db.users;
const Companies = db.companies;

const moment = require('moment');


exports.generate = async (req, res) => {
    // let pdf = require('handlebars-pdf');
    const fs = require('fs');
    let filename = "./test-" + Math.random() + ".pdf";
    // console.log(fs.readFileSync('views/email/email.hbs'))

    let docName = req.body.docName;


    let data = req.body.companyInfo;
    // console.log(req.body)
    data.name = data.companyNameChoices[0].name_choice;
    data.approval = +data.approval;
    data.take_over = +data.take_over;

    let directorData = req.body.personalInfo.shareHoldersDirs[0];
    directorData.date = new Date();
    directorData.email = 'testuser@@gmail.com';
    // console.log(directorData)


    /*
        let document = {
            template: fs.readFileSync('views/docs/form-45-template.hbs').toString(),
            // template: '<h1>{{msg}}</h1>'+
            //     '<p style="color:red">Red text</p>'+
            //     '<img src="https://archive.org/services/img/image" />',
            context: {
                msg: 'Hello world',
                name: data.name,
                code: '1234',
                personal: directorData
            },
            layoutsDir: 'views/docs/',
            path: filename
        };
        //
        //
        //
        pdf.create(document)
            .then(result => {
                // console.log(res)
                // res.render('../views/docs/form-45-template.hbs', { name: data.name})
            })
            .catch(error => {
                // console.log('here', error)
                // console.error(error)
            });
    */

    // const fs = require("fs");
    directorData.full_name = directorData.first_name + ' ' + directorData.last_name;
    console.log(directorData)
    this.writePdf(directorData, data);

    // await Users.findOrCreate({
    //     where: {
    //         email: directorData.email
    //     },
    //     defaults: {
    //         first_name: 'Test',
    //         last_name: 'User',
    //         email: directorData.email,
    //         address: directorData.address_1
    //     },
    // });
    // let u = await Users.findOne({
    //     where: {
    //         email: directorData.email
    //     }
    // });
    //
    // data.director_id = u['id'];
    //
    // console.log(directorData)
    // await Companies.findOrCreate({
    //     where: {
    //         name: data.name
    //     },
    //     defaults: data
    // });

    let companyName = req.body.companyInfo.companyNameChoices[0]['name_choice'];
    let templateName = req.body.docName;

    if (templateName) {

        res.render('../views/docs/' + docName + '.hbs', {
            name: companyName,
            root: process.env.API_URL,
            personal: directorData,
            dated: moment().format('YYYY-MM-DD')
        })
    } else res.status(500).json({msg: 'Please specify a template name'})
};


exports.updateCompanyInfo = async (req, res) => {
    let data = req.body;
    let shareholderData = data.personalInfo.shareHoldersDirs[0];
    shareholderData.full_name = shareholderData.first_name + ' ' + shareholderData.last_name;
    // console.log('update!!!!!')
    // console.log(shareholderData)
    // console.log('!!!!!!!')


    // data.name = data.company;
    this.writePdf(shareholderData, data);
    res.render('../views/docs/' + data.pdfTemplateName + '.hbs', {
        name: data.companyInfo.companyNameChoices[0]['name_choice'],
        personal: shareholderData,
        root: process.env.API_URL,
        dated: moment().format('YYYY-MM-DD')
    })
};

exports.writePdf = async (directorData, data) => {
    const path = require("path");
    const puppeteer = require('puppeteer');
    const handlebars = require("handlebars");
    const fs = require('fs');

    if(data.pdfTemplateName){
        console.log('pdf!!!!')
        // console.log(directorData)
        let dataBinding = {
            name: data.name,
            code: '1234',
            personal: directorData,
            total: 600,
            isWatermark: false,
            dated: moment().format('YYYY-MM-DD'),
            root: process.env.API_URL,
        };

        let templateHtml = fs.readFileSync(path.join(process.cwd(), 'views/docs/' + data.pdfTemplateName + '.hbs'), 'utf8');
        let template = handlebars.compile(templateHtml);
        let finalHtml = template(dataBinding);

        let pdfPath = './public/pdf-docs/'+data.pdfTemplateName+'.pdf';

        let options = {
            format: 'A4',
            headerTemplate: '<p style="font-size: 12px;font-style: italic; margin: 0 55px">Form 45 Continuation Sheet 1</p>',
            footerTemplate: '<p style="font-size: 12px;  margin: 0 auto;"></p>',
            displayHeaderFooter: true,
            margin: {
                bottom: 50, // minimum required for footer msg to display
                left: 70,
                right: 75,
                top: 60,
            },
            printBackground: true,
            path: pdfPath
        };

        const browser = await puppeteer.launch({
            args: ['--no-sandbox','--profile-directory="Default"'],
            headless: true
        });

        const page = await browser.newPage();
        await page.setContent(finalHtml);

        await page.addStyleTag({
            content: "@page:first {margin-top: 0;} body {margin-top: 1cm;}"
        });





        // await page.goto(`data: text/html,${finalHtml}`, {
        //     waitUntil: 'networkidle0'
        // });

        // await page.goto(path.join(process.cwd(), 'views/docs/form-45-template.hbs'))
        // console.log(options)
        let pdf = await page.pdf(options);

        await fs.writeFileSync(pdfPath, pdf);

        if (data.download) {
            console.log('download!!!')
            // await page.pdf({path: 'hn.pdf', format: 'A4'});
            await page._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: pdfPath});

        }

        // await page.screenshot({path: 'example.png'});
        // await browser.close();
    }

    else {

    }


};


exports.getGeneratedPdfTemplate = async (req, res) => {
    let data = req.body;
    let c = await Companies.findOne({
        where: {name: 'Test Company'},
        include: [
            {
                model: Users,
                attributes: [
                    [sequelize.fn('concat', sequelize.col('first_name'), ' ', sequelize.col('last_name')), 'full_name'],
                    'birthday', 'email', 'address', 'phone_number', 'passport_number', 'passport_expiry_date', 'signature']
            }
        ]
    });

    res.render('../views/docs/' + data.name + '.hbs', {
        name: c.name,
        root: process.env.API_URL,
        personal: c.user.toJSON(),
        dated: moment().format('YYYY-MM-DD')
    })
};






exports.downloadPdf = async(req,res)=>{

}
