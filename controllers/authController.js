const sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const db = require('../models');
const Users = db.users;
const Companies = db.companies;


const nodemailer = require('nodemailer');
const constants = require('../config/email-constants');

const showIfErrors = require('../helpers/showIfErrors');

const multer = require('../config/multer');
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const moment = require('moment');

exports.login = async (req, res) => {

    // Checking validation result from express-validator
    if (!showIfErrors(req, res)) {
        // Getting request data and setting user fields to return
        let data = req.body;
        let email = data.email.trim();

        let attributes = [`first_name`, `last_name`, 'email', 'profile_img', 'password', 'id', 'status_id'];

        // Active status selecting
        let statusWhere = sequelize.where(sequelize.col('`users_status`.`name_en`'), 'active');


        // Selecting an employee that has an email matching request one
        let user = await Users.findOne({
            attributes: attributes,
            include: [],
            where: {email: email} //userTypeWhere
        }, res);


        if (!res.headersSent) {


            // User is not active
            if (!user) res.status(500).json({msg: 'You don\'t have such privileges or the account is inactive'});

            else {
                // Cloning users object without password and saving user full name
                let {password, ...details} = user.toJSON();
                let full_name = user[`first_name`] + ' ' + user[`last_name`];


                res.status(200).json({
                    token: jwt.sign(details, 'secretkey', {expiresIn: '8h'}), user_id: user.id, full_name: full_name
                })
            }


        }
    }
};

exports.logout = (req, res) => {
    req.logout();
    res.status(200).json({msg: 'OK'})
};

exports.register = (req, res) => {
    let data = {};

    Reflect.ownKeys(req.body).forEach(key => {
        data[key] = JSON.parse(req.body[key]);
    });

    multer.uploadImages(req, res, async (err) => {

        // Gets file type validation error
        if (req.fileTypeError) {
            res.status(423).json(req.fileTypeError);
        }

        // Getting multer errors if any
        else if (err) res.status(423).json(err);

        // If file validation passed, heading to the request data validation
        else {

            // Getting validation result from express-validator
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json(errors.array()[0]);
            }

            console.log('controller')

            const companyData = {...data.companyInfo, ...data.companyDetails};
            const usersData = data.personalInfo.shareHoldersDirs;
            let counter = 0;
            let list = usersData.map(async (d) => {


                // Saving the original password of user and hashing it to save in db
                let originalPass = d.password;
                d.password = bcrypt.hashSync(originalPass, 10);
                d.address = d.address_1;

                // console.log(d)
                await Users.create(d)
                return d;
            });

            const results = await Promise.all(list);

            companyData.name = companyData.companyNameChoices[0]['name_choice'];
            let firstUser = await Users.findOne({where: {email: results[0].email}})
            companyData.director_id = firstUser.toJSON().id;
            console.log(companyData)
            await Companies.create(companyData);

            res.json('OK');
        }
    })


};


exports.sendConfirmationCodeEmail = (req, res) => {
    if (!showIfErrors(req, res)) {

        //reference the plugin
        const hbs = require('nodemailer-express-handlebars');

        // nodemailer handlebars options
        const options = {
            viewEngine: {
                extname: '.hbs', // handlebars extension
                layoutsDir: 'views/email/', // location of handlebars templates
                defaultLayout: 'email', // name of main template
                partialsDir: 'views/email/partials', // location of your subtemplates aka. header, footer etc
            },
            viewPath: 'views/email/',
            extName: '.hbs'
        };

        //attach the plugin to the nodemailer transporter
        constants.transporter.use('compile', hbs(options));
        constants.confirmationCodeMailOptions.to = req.body.email;


        let pdf = require('handlebars-pdf');
        const fs = require('fs');
        let filename = "./test-" + Math.random() + ".pdf";
        // console.log(fs.readFileSync('views/email/email.hbs'))
        let document = {
            template: fs.readFileSync('views/email/email.hbs').toString(),
            // template: '<h1>{{msg}}</h1>'+
            //     '<p style="color:red">Red text</p>'+
            //     '<img src="https://archive.org/services/img/image" />',
            context: {
                msg: 'Hello world',
                name: 'OK',
                code: '1234'
            },
            path: filename
        };

        pdf.create(document)
            .then(result => {
                // console.log(res)
                // res.render('../views/email/email.hbs', {context: {code: '1234'}})
            })
            .catch(error => {
                console.error(error)
            })

        console.log(constants.confirmationCodeMailOptions)

        // send mail with defined transport object
        constants.transporter.sendMail(constants.confirmationCodeMailOptions, (error, info) => {
            if (error) {
                res.status(500).json({msg: error.toString()})
            } else if (info) {

                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                res.json("OK");
            }


        });

    }


};











// exports.viewDoc = (req, res) => {
//     let data = req.query;
//     res.render('../views/docs/' + data.doc + '.hbs', {name: data.name})
// };


// exports.editDoc = (req, res) => {
//     let data = req.query;
//     res.render('../views/docs/company_test.hbs', {name: data.name, edit: true})
// };


// exports.saveForm45 = (req, res) => {
//     console.log(req.body)
//     res.redirect('http://localhost:4200/auth/new-company');
// };



