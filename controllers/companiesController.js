const db = require('../models');
const Users = db.users;
const Companies = db.companies;

exports.getCompanyInfo = async (req, res) => {
    let c = await Companies.findOne({
        where: {name: 'Test Company'},
        include: [
            {model: Users}
        ]
    });
    res.json(c);
};
