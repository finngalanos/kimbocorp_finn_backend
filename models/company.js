'use strict';
module.exports = (sequelize, DataTypes) => {
    const company = sequelize.define('companies', {
        name: DataTypes.STRING,
        director_id: DataTypes.INTEGER,
        approval: DataTypes.INTEGER,
        take_over: DataTypes.INTEGER,
        company_type: {
            type: DataTypes.STRING
        },
        shares_num:{
            type: DataTypes.INTEGER
        },
        share_capital:{
            type: DataTypes.INTEGER
        },
        currency: {
            type: DataTypes.STRING
        },
    }, {underscored: true});
    company.associate = function (models) {
        company.belongsTo(models.users, {foreignKey: 'director_id'})
        // associations can be defined here
    };
    return company;
};
