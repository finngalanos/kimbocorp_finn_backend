'use strict';
module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        role_id: DataTypes.INTEGER,
        status_id: DataTypes.INTEGER,
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
        birthday: DataTypes.DATEONLY,
        passport_number: {
            type: DataTypes.STRING
        },
        passport_expiry_date: {
            type: DataTypes.DATEONLY
        },
        gender: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        phone_number: DataTypes.STRING,
        password: DataTypes.STRING,
        profile_img: DataTypes.STRING,
        signature: {
            type: DataTypes.STRING
        },
        // access_token: DataTypes.STRING
    }, {timestamps: false});
    users.associate = function (models) {

        // associations can be defined here
    };
    return users;
};
