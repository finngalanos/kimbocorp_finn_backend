const express = require('express');
const exphbs = require('express-handlebars');
const app = express();

const port = process.env.PORT || 3000;
const server = require('http').createServer(app);
const cors = require('cors');
const constants = require('./config/constants');


// Cors
app.use(cors(require('./config/cors')));

const bodyParser = require('body-parser');

// Start server on pre-defined port
server.listen(port, () => {
    console.log('server is listening on port ' + port)
});

// Dotenv used to read process.env
require('dotenv').config();


// Body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// Static resources
app.use(express.static(__dirname + '/public'));
app.set('views', require('path').join(__dirname, '/views'));
app.engine('handlebars', exphbs({extname: '.hbs'}));
app.set('view engine', 'handlebars');

// Registering if helper
const hbs = require('hbs');
hbs.registerHelper('if_eq', (a, b, opts) => {
    if (a == b)
        return opts.fn(this);
    else
        return opts.inverse(this);
});


// Passport.js config
// const passport = require('passport');
// require('./config/google-passport-strategy')(passport);
// require('./config/facebook-passport-strategy')(passport);
// app.use(passport.initialize({}));

// Routes
app.use('/auth', require('./routes/auth'));
app.use('/docs', require('./routes/docs'));
app.use('/api/user', require('./routes/auth'));


const path = require('path');


let dist = path.join(__dirname, '/dist/');

if (process.env.NODE_ENV === 'production') {
    dist = path.join(__dirname, '/dist/')
}
app.use(express.static(dist));

// Separating Angular routes
app.get('*', (req, res, next) => {
    if (!req.url.includes('phpmyadmin')) {
        res.sendFile(dist + 'index.html');
    }
});



