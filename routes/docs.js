const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const docsController = require('../controllers/docsController');
const companiesController = require('../controllers/companiesController');

// Regular auth routes and social auth logout route
router.post('/generate', docsController.generate);
// router.post('/view', authController.viewDoc);
router.post('/edit', docsController.getGeneratedPdfTemplate);
// router.post('/save-form', authController.saveForm45);
router.get('/get-company-info', companiesController.getCompanyInfo);
router.put('/update-company-info', docsController.updateCompanyInfo);
// router.post('/send-confirmation-code', validateSendConfirmation.rules, authController.sendConfirmationCodeEmail);
// router.post('/login', validateLogin.rules, authController.login);
// router.get('/logout', authController.logout);




module.exports = router;

